const { MESSAGE } = require("../static/js/constants");

const roomState = {};
const changedRooms = {};
const getBlankState = () => ({
  notes: {},
});

module.exports = (io) => {
  io.on("connection", (socket) => {
    socket.on(MESSAGE.JOIN, ({ roomName }) => {
      if (!roomState[roomName]) roomState[roomName] = getBlankState();
      socket.join(roomName);
      socket.emit(MESSAGE.STATE_CHANGE, roomState[roomName]);
      socket.on(MESSAGE.STATE_CHANGE, (newState) => {
        roomState[roomName] = newState;
        changedRooms[roomName] = true;
      });
    });
  });

  // Update interval
  setInterval(() => {
    const changedNames = Object.keys(changedRooms);
    changedNames.forEach((name) => {
      if (changedRooms[name] !== true) return;
      changedRooms[name] = false;
      io.to(name).emit(MESSAGE.STATE_CHANGE, roomState[name]);
    });
  }, 300);
};
