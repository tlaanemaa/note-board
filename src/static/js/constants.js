// Constants
const constants = {
  MESSAGE: {
    JOIN: "join-room",
    STATE_CHANGE: "state-change",
  },
};

// Wonky way to allow using this in Node.js and browser
if (typeof module === "object") module.exports = constants;
