const { MESSAGE } = constants;

// This is where we keep our shared state
let pageState = {
  notes: {},
};

// This indicates which note is being moved around
let noteBeingMoved = null;

// Container where we kee our notes
const notesParent = document.getElementById("notes");

class StateHandler {
  constructor() {
    this.notes = {};

    this.setState = this.setState.bind(this);
    this.createNote = this.createNote.bind(this);
  }

  setState(state) {
    pageState = state;
    const newNotes = Object.keys(pageState.notes);
    const currentNotes = Object.keys(this.notes);

    const addedNotes = newNotes.filter((id) => !currentNotes.includes(id));
    const removedNotes = currentNotes.filter((id) => !newNotes.includes(id));

    removedNotes.forEach((id) => {
      this.notes[id].nuke();
      delete this.notes[id];
    });

    addedNotes.forEach((id) => {
      this.createNote(pageState.notes[id]);
    });

    Object.values(this.notes).forEach((note) => note.render());
  }

  createNote(state) {
    const note = new Note(state);
    this.notes[note.id] = note;
    note.init();
  }
}

class Note {
  constructor({ id = uuidv4(), x = 100, y = 100, text = "" } = {}) {
    this.id = id;
    this.state = { id, x, y, text };
    this.startTracking = this.startTracking.bind(this);
    this.handleMousemove = this.handleMousemove.bind(this);
    this.handleTextChange = this.handleTextChange.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.nuke = this.nuke.bind(this);
    pushState();
  }

  // We keep our state externally to make it easy to send over network
  get state() {
    return pageState.notes[this.id];
  }

  set state(value) {
    pageState.notes[this.id] = value;
  }

  init() {
    if (this.node) return;

    this.node = document.createElement("div");
    this.node.classList.add("note");
    this.node.classList.add("smooth");
    this.node.addEventListener("mousedown", this.startTracking);
    this.node.addEventListener("mouseup", this.stopTracking);

    this.textarea = document.createElement("textarea");
    this.textarea.addEventListener("keyup", this.handleTextChange);
    this.node.appendChild(this.textarea);

    const deleteBtn = document.createElement("button");
    deleteBtn.textContent = "X";
    deleteBtn.addEventListener("click", this.handleDelete);
    this.node.appendChild(deleteBtn);

    notesParent.appendChild(this.node);

    this.render();
  }

  handleTextChange() {
    this.state.text = this.textarea.value;
    pushState();
  }

  handleDelete() {
    delete pageState.notes[this.id];
    this.nuke();
    pushState();
  }

  startTracking(e) {
    if (e.button !== 0) return;
    this.grabOffset = {
      x: e.clientX - this.state.x,
      y: e.clientY - this.state.y,
    };
    if (this.node) this.node.classList.remove("smooth");
    noteBeingMoved = this;
  }

  stopTracking() {
    if (this.node) this.node.classList.add("smooth");
    this.grabOffset = null;
  }

  handleMousemove(e) {
    const newX = e.clientX - this.grabOffset.x;
    const newY = e.clientY - this.grabOffset.y;
    const padding = 5;
    this.state.x = Math.max(
      Math.min(
        newX,
        document.body.offsetWidth - this.node.offsetWidth - padding
      ),
      padding
    );
    this.state.y = Math.max(
      Math.min(
        newY,
        document.body.offsetHeight - this.node.offsetHeight - padding
      ),
      padding
    );
    pushState();
    this.render();
  }

  render() {
    this.node.style = `transform: translate(${this.state.x}px, ${this.state.y}px)`;
    if (this.textarea !== document.activeElement)
      this.textarea.value = this.state.text;
  }

  nuke() {
    if (this.textarea) this.textarea.remove();
    if (this.node) this.node.remove();
    this.textarea = null;
    this.node = null;
  }
}

// Note moving event handlers
document.addEventListener("mousemove", (e) => {
  if (noteBeingMoved) noteBeingMoved.handleMousemove(e);
});

document.addEventListener("mouseup", () => {
  if (noteBeingMoved) noteBeingMoved.stopTracking();
  noteBeingMoved = null;
});

// Connect socket
const socket = io();
const stateHandler = new StateHandler();
socket.emit(MESSAGE.JOIN, { roomName: document.location.pathname });
socket.on(MESSAGE.STATE_CHANGE, stateHandler.setState);
const pushState = () => socket.emit(MESSAGE.STATE_CHANGE, pageState);

// Click handler to create a new note
document
  .getElementById("add")
  .addEventListener("click", () => stateHandler.createNote());
