const path = require("path");
const express = require("express");
const app = express();
const http = require("http").createServer(app);
const io = require("socket.io")(http);
require("./api")(io);

app.use("/static", express.static(path.resolve(__dirname, "static")));
app.use("/:roomName", (req, res) =>
  res.sendFile(path.resolve(__dirname, "static", "index.html"))
);
app.use((req, res) =>
  res.sendFile(path.resolve(__dirname, "static", "404.html"))
);
http.listen(8080, () => console.log("Listening on http://localhost:8080/"));
